import {helloWorld} from "../../src";

describe(`TS Workspace Templace`, function(){
    it(`Calls Hello World`, function(){
       expect(helloWorld()).toEqual(`Hello World`)
    });
});