![Node Version](https://img.shields.io/badge/dynamic/json.svg?url=https%3A%2F%2Fraw.githubusercontent.com%2Fbadges%2Fshields%2Fmaster%2Fpackage.json&label=Node&query=$.engines.node&colorB=blue)
![NPM Version](https://img.shields.io/badge/dynamic/json.svg?url=https%3A%2F%2Fraw.githubusercontent.com%2Fbadges%2Fshields%2Fmaster%2Fpackage.json&label=NPM&query=$.engines.npm&colorB=purple)


## Typescript Project Workspace Template

With documentation, release mechanism and github workflows to match

### Installation

In order to use this project as a dependency

##### Step 1: Run npm install

To install as a dependency do:
```sh
$ npm install @noname-project/nameofpackage
```

To install as a dev dependency do:
```sh
$ npm install @noname-project/nameofpackage --save-dev
```
instead.



### Repository Structure

```
ts-template
│
│   .gitignore              <-- Defines files ignored to git
│   .gitlab-ci.yml          <-- GitLab CI/CD config file
│   .nmpignore              <-- Defines files ignored by npm
│   .nmprc                  <-- Defines the Npm registry for this package
│   gulpfile.js             <-- Gulp build scripts. used in the 'build' and 'build:prod' npm scripts
│   jest.config.js          <-- Tests Configuration file
│   jsdocs.json             <-- Documentation generation configuration file
│   LICENCE.md              <-- Licence disclamer
│   nodemon.json            <-- Nodemon config file (allows to live test ts files)
│   package.json
│   package-lock.json
│   README.md               <-- Readme File dynamically compiled from 'workdocs' via the 'docs' npm script
│   tsconfig.json           <-- Typescript config file. Is overriden in 'gulpfile.js' 
│
└───bin
│   │   tag_release.sh      <-- Script to help with releases
│   
└───docs
│   │   ...                 <-- Dinamically generated folder, containing the compiled documentation for this repository. generated via the 'docs' npm script
│   
└───src
│   │   ...                 <-- Source code for this repository
│   
└───tests
│   │   ...                 <-- Test sources for this repository
│   
└───workdocs                <-- Folder with all pre-compiled documentation
    │   ...
    │   Readme.md           <-- Entry point to the README.md   
```


<!--
### Social

[![LinkedIn](https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white)](https://pt.linkedin.com/company/pdmfc)


#### Disclaimer:

![Disclamer](https://static.wixstatic.com/media/2844e6_69acaab42d5a47c9a20a187b384741ef~mv2.png/v1/fill/w_531,h_65,al_c,q_85,usm_0.66_1.00_0.01/2021-01-21_11-27-05_edited.webp)

-->